package com.alexandro46.vkfriendsmonitor;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

import javax.net.ssl.HttpsURLConnection;
import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.URL;
import java.util.Date;

public class Main {

    private LogWriter lw = new LogWriter();

    private Main() {
        lw.init();
        new NoName().start();
    }

    private class NoName extends Thread {

        String token;

        @Override
        public void run() {
            token = Auth.getTokenUrl("https://oauth.vk.com/authorize?client_id=5891543&display=page&redirect_uri=https://oauth.vk.com/blank.html&scope=friends&response_type=token&v=5.62&state=123456");
            do {
                String json = request("https://api.vk.com/method/friends.getOnline?access_token=" + token + "&v=5.62");
                JSONParser parser = new JSONParser();
                try {
                    JSONObject o = (JSONObject) parser.parse(json);
                    JSONArray arr = (JSONArray) o.get("response"); // масив id
                    Date d = new Date(System.currentTimeMillis());
                    StringBuilder sb = new StringBuilder();
                    for (Object anArr : arr) {
                        sb.append(anArr + ","); //постройка строки (id,id,id,...)
                    }
                    String json2 = request("https://api.vk.com/method/users.get?user_ids=" + sb.toString() + "&v=5.62"); // json з іменами
                    JSONObject o2 = (JSONObject) parser.parse(json2);
                    JSONArray arr2 = (JSONArray) o2.get("response");
                    StringBuilder sb2 = new StringBuilder(d.toString() + " : ");
                    for(int j = 0;j < arr2.size();j++) {
                        JSONObject obj = (JSONObject) arr2.get(j);
                        sb2.append(obj.get("first_name") + " " + obj.get("last_name")); // постройка строки (date : id,id,id...)
                        if(j != (arr2.size() - 1)) sb.append(", "); // для красоти
                    }
                    lw.write(sb2.toString()); // пишу в файл і в консоль
                    System.out.println(sb2.toString());
                } catch (ParseException e) {
                    e.printStackTrace();
                }
                if(exit()) break; // якщо тре вийти то перериваю цикл
                try {
                    Thread.sleep(60000 * 5); // чекаю 5 минут
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            } while (!exit()); //ще раз провіраю чи не треба вийти
            lw.closePW();
        }

        String request(String url) { // request (рекв`ест) з англ. запит
            String response; // response (респ`онс) з англ. відповідь
            try {
                HttpsURLConnection connection = (HttpsURLConnection) new URL(url).openConnection();
                connection.setRequestMethod("GET");
                connection.setRequestProperty("User-Agent","Fr.Mon.");
                response = new BufferedReader(new InputStreamReader(connection.getInputStream())).readLine();
            } catch (IOException e) {
                e.printStackTrace();
                response = "ERROR";
            }
            return response;
        }

        boolean exit() { // провірка вихода. Поки зробив так, потім щось придумаю
            try {
                if(new BufferedReader(new InputStreamReader(new FileInputStream("exit"))).readLine().equals("f")) return false;
            } catch (IOException e) {
                e.printStackTrace();
            }
            return true;
        }
    }

    public static void main(String[] args) {
        new Main();
    }
}
