package com.alexandro46.vkfriendsmonitor;

import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.PrintWriter;

/**
 * Created by Саша on 23.02.2017.
 *
 */
class LogWriter {
    //TODO : файл записується після виконання "closePW()", тоді якщо програма завершиться з помилкою данні не буде збережено

    private static PrintWriter pw;

    //ініціалізація PrintWriter
    void init() {
        try {
            pw = new PrintWriter(new FileOutputStream(System.currentTimeMillis() + ".txt"),false);
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
    }

    //запис строк
    void write(String s) {
        pw.println(s);
    }

    //закритя PrintWriter (див. TOD0 на 12 стр.)
    void closePW() {
        pw.close();
    }
}
