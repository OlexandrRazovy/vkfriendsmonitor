package com.alexandro46.vkfriendsmonitor;

import javafx.application.Application;
import javafx.scene.Scene;
import javafx.scene.web.WebEngine;
import javafx.scene.web.WebView;
import javafx.stage.Stage;

/**
 * Created by Саша on 23.02.2017.
 *
 */
public class Auth extends Application {

    private static final String REDIRECT_URL = "https://oauth.vk.com/blank.html";
    private static String VK_AUTH_URL = "https://oauth.vk.com/authorize?client_id=5695363&display=page&redirect_uri=https://oauth.vk.com/blank.html&scope=audio&response_type=token&v=5.59"; //TODO!!!
    private static String tokenUrl;

    static String getTokenUrl(String url) {
        VK_AUTH_URL = url;
        launch(Auth.class);
        return tokenUrl;
    }

    @Override
    public void start(Stage primaryStage) throws Exception {
        final WebView view = new WebView();
        final WebEngine engine = view.getEngine();
        engine.load(VK_AUTH_URL);


        primaryStage.setScene(new Scene(view));
        primaryStage.show();

        engine.locationProperty().addListener((observable, oldValue, newValue) -> {
            if (newValue.startsWith(REDIRECT_URL)) {
                tokenUrl = newValue;
                StringBuilder sb = new StringBuilder(tokenUrl);
                tokenUrl = sb.substring(45,130);
                primaryStage.close();
            }
        });

    }
}